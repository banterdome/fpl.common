﻿using FPL.Common.DataAccess.DataContexts;

namespace FPL.Common.DataAccess.DataContext
{
    public interface IDataContextFactory
    {
        IFPLDataContext Create();
    }
}
