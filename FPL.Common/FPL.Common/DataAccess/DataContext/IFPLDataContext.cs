﻿using FPL.Common.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.DataContexts
{
    public interface IFPLDataContext : IDisposable
    {
        DbSet<Token> Tokens { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }
        DbSet<SubscriptionType> SubscriptionTypes { get; set; }
        DbSet<Event> Events { get; set; }
        DbSet<Person> Persons { get; set; }
        DbSet<Title> Titles { get; set; }
        DbSet<TitleType> TitleTypes { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        Task<int> SaveChangesAsync();
    }
}
