﻿using FPL.Common.DataAccess.DataContexts;
using FPL.Common.Settings;
using Microsoft.EntityFrameworkCore;

namespace FPL.Common.DataAccess.DataContext
{
    public class DataContextFactory : IDataContextFactory
    {
        private DatabaseSettings databaseSettings { get; set; }

        public DataContextFactory(DatabaseSettings databaseSettings)
        {
            this.databaseSettings = databaseSettings;
        }

        public IFPLDataContext Create()
        {
            var contextOptionsBuilder = new DbContextOptionsBuilder();
            contextOptionsBuilder.UseSqlServer(databaseSettings.ConnectionString);

            return new FPLDataContext(contextOptionsBuilder.Options);
        }
    }
}