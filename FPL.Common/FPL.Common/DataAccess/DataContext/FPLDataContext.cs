﻿using FPL.Common.DataAccess.DataContexts;
using FPL.Common.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.DataContext
{
    public class FPLDataContext : DbContext, IFPLDataContext
    {
        public FPLDataContext(DbContextOptions options) : base(options) { }

        public DbSet<Token> Tokens { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<SubscriptionType> SubscriptionTypes { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<TitleType> TitleTypes { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
