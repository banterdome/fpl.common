﻿using FPL.Common.DataAccess.Commands.Interfaces;
using FPL.Common.DataAccess.DataContext;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.Commands
{
    public class DeleteEntityCommand : IDeleteEntityCommand
    {
        private readonly IDataContextFactory dataContextFactory;

        public DeleteEntityCommand(IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task Execute<T>(T entity) where T : class
        {
            using (var context = this.dataContextFactory.Create())
            {
                context.Set<T>().Remove(entity);
                await context.SaveChangesAsync();
            }
        }
    }
}