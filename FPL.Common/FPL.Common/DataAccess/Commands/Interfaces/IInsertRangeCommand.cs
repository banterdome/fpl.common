﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.Commands.Interfaces
{
    public interface IInsertRangeCommand
    {
        Task Execute<T>(List<T> entity) where T : class;
    }
}
