﻿using System.Threading.Tasks;

namespace FPL.Common.DataAccess.Commands.Interfaces
{
    public interface IDeleteEntityCommand
    {
        Task Execute<T>(T entity) where T : class;
    }
}
