﻿using FPL.Common.DataAccess.Commands.Interfaces;
using FPL.Common.DataAccess.DataContext;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.Commands
{
    public class InsertRangeCommand : IInsertRangeCommand
    {
        private readonly IDataContextFactory dataContextFactory;

        public InsertRangeCommand(IDataContextFactory dataContextFactory)
        {
            this.dataContextFactory = dataContextFactory;
        }

        public async Task Execute<T>(List<T> entity) where T : class
        {
            using (var context = this.dataContextFactory.Create())
            {
                await context.Set<T>().AddRangeAsync(entity);
                await context.SaveChangesAsync();
            }
        }
    }
}
