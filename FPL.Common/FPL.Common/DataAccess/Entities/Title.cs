﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("FT_Fantasy_Titles", Schema = "dbo")]
    public class Title
    {
        [Key, Column("FT_KEY")]
        public int Key { get; set; }

        [Column("FT_FTT_KEY")]
        public int TitleTypeKey { get; set; }

        [Column("FT_YEAR")]
        public string Year { get; set; }

        [Column("FT_WINNING_PERSON")]
        public int WinningPerson { get; set; }

        [ForeignKey("WinningPerson")]
        public Person Person { get; set; }

        [ForeignKey("TitleTypeKey")]
        public TitleType TitleType { get; set; }
    }
}
