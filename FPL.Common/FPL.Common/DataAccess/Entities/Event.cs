﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("FLE_Fantasy_League_Event", Schema = "dbo")]
    public class Event
    {
        [Key, Column("FLE_Key")]
        public int Key { get; set; }

        [Column("FLE_Player")]
        public int Player { get; set; }
        [Column("FLE_Amount")]
        public int Amount { get; set; }
        [Column("FLE_GameID")]
        public int GameId { get; set; }
        [Column("FLE_Event_Type")]
        public int EventType { get; set; }

        [NotMapped]
        public List<string> Tokens { get; set; }
    }
}
