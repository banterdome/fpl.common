﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;


namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("FTT_Fantasy_Title_Types", Schema = "dbo")]

    public class TitleType
    {
        [Key, Column("FTT_Key")]
        public int Key { get; set; }

        [Column("FTT_NAME")]
        public string Name { get; set; }
    }
}
