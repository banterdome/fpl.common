﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("NST_NOTIFICATION_SUBSCRIPTION_TYPE", Schema = "dbo")]
    public class SubscriptionType
    {
        [Key, Column("NST_key")]
        public int Key { get; set; }

        [Column("NST_NS_Key")]
        public int SubscriptionKey { get; set; }

        [Column("NST_NT_Key")]
        public int NotificationTypeKey { get; set; }

        [ForeignKey("SubscriptionKey")]
        public Subscription Subscription { get; set; }
    }
}
