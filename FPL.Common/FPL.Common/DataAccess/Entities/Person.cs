﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("PER_PERSONS", Schema = "dbo")]
    public class Person
    {
        [Key, Column("PER_KEY")]
        public int Key { get; set; }

        [Column("PER_NAME")]
        public string Name { get; set; }

        public List<Title> TitleWins { get; set; }
    }
}
