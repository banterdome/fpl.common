﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("NS_NOTIFICATION_SUBSCRIPTIONS", Schema = "dbo")]
    public class Subscription
    {
        [Key, Column("NS_Key")]
        public int Key { get; set; }

        [Column("NS_Subscriber")]
        public int Subscriber { get; set; }

        [Column("NS_Player")]
        public int Player { get; set; }

        public List<SubscriptionType> SubscriptionTypes { get; set; }
    }
}
