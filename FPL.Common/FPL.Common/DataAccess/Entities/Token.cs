﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Common.DataAccess.Entities
{
    [ExcludeFromCodeCoverage]
    [Table("TO_TOKENS", Schema = "dbo")]
    public class Token
    {
        [Key, Column("TO_Key")]
        public int Key { get; set; }

        [Column("TO_Token")]
        public string FirebaseToken { get; set; }

        [Column("TO_LOG_key")]
        public int LoginKey { get; set; }
    }
}