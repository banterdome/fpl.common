﻿using RestSharp;

namespace FPL.Common.Requests.Interfaces
{
    public interface IRestRequestFactory
    {
        IRestRequest Create(string endpoint);
    }
}
