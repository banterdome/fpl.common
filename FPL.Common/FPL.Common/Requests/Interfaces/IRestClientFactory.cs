﻿using RestSharp;

namespace FPL.Common.Requests.Interfaces
{
    public interface IRestClientFactory
    {
        IRestClient Create(string URL);
    }
}
