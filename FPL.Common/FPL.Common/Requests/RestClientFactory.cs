﻿using FPL.Common.Requests.Interfaces;
using RestSharp;

namespace FPL.Common.Requests
{
    public class RestClientFactory : IRestClientFactory
    {
        public IRestClient Create(string url)
        {
            return new RestClient(url);
        }
    }

}
