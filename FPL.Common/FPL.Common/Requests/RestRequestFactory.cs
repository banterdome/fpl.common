﻿using FPL.Common.Requests.Interfaces;
using RestSharp;

namespace FPL.Common.Requests
{
    public class RestRequestFactory : IRestRequestFactory
    {
        public IRestRequest Create(string endpoint)
        {
            return new RestRequest(endpoint, Method.GET);
        }
    }
}

