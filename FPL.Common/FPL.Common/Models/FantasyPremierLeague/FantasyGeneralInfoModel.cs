﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyGeneralInfoModel
    {
        [JsonProperty("events")]
        public List<FantasyGameweekModel> Gameweeks { get; set; }

        [JsonProperty("teams")]
        public List<FantasyTeamModel> Teams { get; set; }

        [JsonProperty("elements")]
        public List<FantasyPlayerModel> Players { get; set; }

        public FantasyGameweekModel CurrentGameweek { get { return this.Gameweeks.Where(x => x.Current).FirstOrDefault(); } }
    }
}
