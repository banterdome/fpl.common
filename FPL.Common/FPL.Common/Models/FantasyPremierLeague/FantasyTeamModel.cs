﻿namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyTeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
