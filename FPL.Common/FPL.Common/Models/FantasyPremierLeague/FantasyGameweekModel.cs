﻿using Newtonsoft.Json;
using System;

namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyGameweekModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("is_current")]
        public bool Current { get; set; }

        [JsonProperty("finished")]
        public bool Finished { get; set; }

        [JsonProperty("is_next")]
        public bool Next { get; set; }

        [JsonProperty("deadline_time")]
        public DateTime Deadline { get; set; }
    }
}
