﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyGameModel
    {
        [JsonProperty("code")]
        public int Key { get; set; }

        [JsonProperty("team_h")]
        public int HomeTeam { get; set; }

        [JsonProperty("team_a")]
        public int AwayTeam { get; set; }

        [JsonProperty("finished_provisional")]
        public bool FinishedProvisional { get; set; }

        [JsonProperty("kickoff_time")]
        public DateTime KickOffTime { get; set; }

        [JsonProperty("started")]
        public bool Started { get; set; }

        [JsonProperty("team_h_score")]
        public int? HomeScore { get; set; }

        [JsonProperty("team_a_score")]
        public int? AwayScore { get; set; }

        [JsonProperty("stats")]
        public List<FantasyStatsModel> Stats { get; set; }
    }
}
