﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyStatsModel
    {
        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("h")]
        public List<Value> Home { get; set; }

        [JsonProperty("a")]
        public List<Value> Away { get; set; }
    }

    public class Value
    {
        [JsonProperty("value")]
        public int Number { get; set; }

        [JsonProperty("element")]
        public int Player { get; set; }
    }
}
