﻿using FPL.Common.Enums;
using Newtonsoft.Json;
using System;

namespace FPL.Common.Models.FantasyPremierLeague
{
    public class FantasyPlayerModel
    {
        public int Id { get; set; }
        public int Team { get; set; }

        [JsonProperty("web_name")]
        public string Name { get; set; }

        [JsonProperty("element_type")]
        public int ElementType { get; set; }

        [JsonProperty("stats")]
        public StatsModel Stats { get; set; }

        public string Position { get { return Enum.GetName(typeof(PositionEnum), ElementType); } }
    }

    public class StatsModel
    {
        [JsonProperty("total_points")]
        public int Points { get; set; }
    }
}
