﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Common.Settings
{
    public record FantasyPremierLeagueSettings(string FplApiUrl, string FplDraftApiUrl, int? LeagueId)
    {
    }
}