﻿using FPL.Common.Getters.Interfaces;
using FPL.Common.Models.FantasyPremierLeague;
using FPL.Common.Requests.Interfaces;
using FPL.Common.Settings;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FPL.Common.Getters
{
    public class GeneralInfoGetter : IGeneralInfoGetter
    {
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;
        private readonly IRestClientFactory restClientFactory;
        private readonly IRestRequestFactory restRequestFactory;

        public GeneralInfoGetter(FantasyPremierLeagueSettings fantasyPremierLeagueSettings, IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory)
        {
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
            this.restClientFactory = restClientFactory;
            this.restRequestFactory = restRequestFactory;
        }

        public async Task<FantasyGeneralInfoModel> GetGeneralInfo()
        {
            var request = this.restRequestFactory.Create($"/bootstrap-static/");

            var client = this.restClientFactory.Create(fantasyPremierLeagueSettings.FplApiUrl);

            var result = await client.ExecuteAsync(request);

            if (result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                var response = JsonConvert.DeserializeObject<FantasyGeneralInfoModel>(result.Content);

                return response;
            }
            else
            {
                throw new Exception($"Failed to get general info, Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
