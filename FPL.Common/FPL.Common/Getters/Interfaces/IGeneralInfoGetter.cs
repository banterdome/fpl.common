﻿using FPL.Common.Models.FantasyPremierLeague;
using System.Threading.Tasks;

namespace FPL.Common.Getters.Interfaces
{
    public interface IGeneralInfoGetter
    {
        public Task<FantasyGeneralInfoModel> GetGeneralInfo();
    }
}
