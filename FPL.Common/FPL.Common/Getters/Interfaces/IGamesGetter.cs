﻿using FPL.Common.Models.FantasyPremierLeague;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Common.Getters.Interfaces
{
    public interface IGamesGetter
    {
        public Task<List<FantasyGameModel>> Get(int gameweek, bool draft);
    }
}
