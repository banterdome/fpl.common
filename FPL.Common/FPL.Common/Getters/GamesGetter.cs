﻿using FPL.Common.Getters.Interfaces;
using FPL.Common.Models.FantasyPremierLeague;
using FPL.Common.Requests.Interfaces;
using FPL.Common.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FPL.Common.Getters
{
    public class GamesGetter : IGamesGetter
    {
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;
        private readonly IRestClientFactory restClientFactory;
        private readonly IRestRequestFactory restRequestFactory;

        public GamesGetter(FantasyPremierLeagueSettings fantasyPremierLeagueSettings, IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory)
        {
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
            this.restClientFactory = restClientFactory;
            this.restRequestFactory = restRequestFactory;
        }

        public async Task<List<FantasyGameModel>> Get(int gameweek, bool draft)
        {
            var request = this.restRequestFactory.Create($"/fixtures/?event={gameweek}");

            var client = this.restClientFactory.Create(draft ? fantasyPremierLeagueSettings.FplDraftApiUrl : fantasyPremierLeagueSettings.FplApiUrl);

            var result = await client.ExecuteAsync(request);

            if (result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                var response = JsonConvert.DeserializeObject<List<FantasyGameModel>>(result.Content);

                return response;
            }
            else
            {
                throw new Exception($"Failed to get games for GW {gameweek}, Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
