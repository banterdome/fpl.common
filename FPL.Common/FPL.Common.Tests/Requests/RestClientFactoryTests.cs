using FPL.Common.Requests;
using Moq.AutoMock;
using RestSharp;
using System;
using Xunit;

namespace FPL.Common.Tests.Requests
{
    public class RestClientFactoryTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateCreatesRestClient()
        {
            var stubUrl = "https://testurl.co.uk/";

            var sut = this.autoMocker.CreateInstance<RestClientFactory>();

            var result = sut.Create(stubUrl);

            Assert.IsType<RestClient>(result);
            Assert.Equal(stubUrl, result.BaseUrl.ToString());
        }
    }

}
