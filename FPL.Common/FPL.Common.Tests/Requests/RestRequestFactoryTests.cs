﻿using FPL.Common.Requests;
using Moq.AutoMock;
using RestSharp;
using Xunit;

namespace FPL.Common.Tests.Requests
{
    public class RestRequestFactoryTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateReturnsCorrectRequest()
        {
            var stubEndpoint = "/test/tests/";

            var sut = this.autoMocker.CreateInstance<RestRequestFactory>();

            var result = sut.Create(stubEndpoint);

            Assert.IsType<RestRequest>(result);
            Assert.Equal(stubEndpoint, result.Resource);
            Assert.Equal(Method.GET, result.Method);

        }
    }
}
