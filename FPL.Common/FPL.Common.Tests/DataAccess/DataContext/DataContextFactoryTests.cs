﻿using Moq.AutoMock;
using Xunit;

namespace FPL.Common.Tests.DataAccess.DataContext
{
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.Settings;
    using Moq.AutoMock;
    using Xunit;

    public class DataContextFactoryTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateReturnsDataContext()
        {
            var stubSettings = new DatabaseSettings("Test");
            this.autoMocker.Use(stubSettings);

            var sut = this.autoMocker.CreateInstance<DataContextFactory>();

            var result = sut.Create();

            Assert.IsType<FPLDataContext>(result);
        }
    }
}
