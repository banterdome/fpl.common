﻿using FPL.Common.Getters;
using FPL.Common.Requests.Interfaces;
using FPL.Common.Settings;
using Moq;
using Moq.AutoMock;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Common.Tests.Getters
{
    public class GamesGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetGeneralInfoReturnsCorrectInformation()
        {
            //arrange
            var stubResponseJson = "[{\"code\":2210363,\"event\":10,\"finished\":true,\"finished_provisional\":true,\"id\":93,\"kickoff_time\":\"2021-10-30T11:30:00Z\",\"minutes\":90,\"provisional_start_time\":false,\"started\":true,\"team_a\":1,\"team_a_score\":2,\"team_h\":9,\"team_h_score\":0,\"stats\":[{\"identifier\":\"goals_scored\",\"a\":[{\"value\":1,\"element\":21},{\"value\":1,\"element\":23}],\"h\":[]},{\"identifier\":\"assists\",\"a\":[{\"value\":1,\"element\":22}],\"h\":[]},{\"identifier\":\"own_goals\",\"a\":[],\"h\":[]},{\"identifier\":\"penalties_saved\",\"a\":[],\"h\":[]},{\"identifier\":\"penalties_missed\",\"a\":[],\"h\":[]},{\"identifier\":\"yellow_cards\",\"a\":[],\"h\":[{\"value\":1,\"element\":201},{\"value\":1,\"element\":205}]},{\"identifier\":\"red_cards\",\"a\":[],\"h\":[]},{\"identifier\":\"saves\",\"a\":[{\"value\":8,\"element\":559}],\"h\":[{\"value\":4,\"element\":200}]},{\"identifier\":\"bonus\",\"a\":[{\"value\":3,\"element\":23},{\"value\":2,\"element\":559},{\"value\":1,\"element\":21}],\"h\":[]},{\"identifier\":\"bps\",\"a\":[{\"value\":41,\"element\":23},{\"value\":38,\"element\":559},{\"value\":30,\"element\":21},{\"value\":25,\"element\":590},{\"value\":19,\"element\":22},{\"value\":18,\"element\":67},{\"value\":18,\"element\":466},{\"value\":14,\"element\":15},{\"value\":9,\"element\":478},{\"value\":5,\"element\":4},{\"value\":5,\"element\":558},{\"value\":3,\"element\":11},{\"value\":3,\"element\":17},{\"value\":2,\"element\":6}],\"h\":[{\"value\":26,\"element\":210},{\"value\":21,\"element\":200},{\"value\":16,\"element\":217},{\"value\":15,\"element\":209},{\"value\":13,\"element\":219},{\"value\":10,\"element\":458},{\"value\":9,\"element\":208},{\"value\":9,\"element\":212},{\"value\":5,\"element\":201},{\"value\":4,\"element\":455},{\"value\":3,\"element\":213},{\"value\":2,\"element\":587},{\"value\":-1,\"element\":205},{\"value\":-1,\"element\":215}]}],\"team_h_difficulty\":3,\"team_a_difficulty\":3,\"pulse_id\":66434}]";
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubGameweek = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GamesGetter>();
            var result = await sut.Get(stubGameweek, true);

            //assert
            Assert.NotNull(result);
            Assert.Equal(2210363, result[0].Key);
            Assert.Equal(0, result[0].HomeScore);
        }

        [Theory]
        [InlineData(401, "test")]
        [InlineData(200, null)]
        public async Task GetGeneralInfoThrowsErrorIfRequestIsUnsuccessful(int statusCode, string response)
        {
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns((HttpStatusCode)statusCode);
            stubResponse.Setup(x => x.Content)
                .Returns(response);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GamesGetter>();

            //assert
            await Assert.ThrowsAsync<Exception>(async () => await sut.Get(1, true));
        }
    }
}
