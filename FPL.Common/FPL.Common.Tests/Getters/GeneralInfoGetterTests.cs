﻿using FPL.Common.Getters;
using FPL.Common.Requests.Interfaces;
using FPL.Common.Settings;
using Moq;
using Moq.AutoMock;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FPL.Common.Tests.Getters
{
    public class GeneralInfoGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetGeneralInfoReturnsCorrectInformation()
        {
            //arrange
            var stubResponseJson = "{\"events\":[{\"id\":1,\"is_current\":false},{\"id\":2,\"is_current\":true}]}";
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GeneralInfoGetter>();
            var result = await sut.GetGeneralInfo();

            //assert
            Assert.NotNull(result);
            Assert.Equal(1, result.Gameweeks[0].Id);
            Assert.False(result.Gameweeks[0].Current);
        }

        [Theory]
        [InlineData(401, "test")]
        [InlineData(200, null)]
        public async Task GetGeneralInfoThrowsErrorIfRequestIsUnsuccessful(int statusCode, string response)
        {
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns((HttpStatusCode)statusCode);
            stubResponse.Setup(x => x.Content)
                .Returns(response);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GeneralInfoGetter>();

            //assert
            await Assert.ThrowsAsync<Exception>(async () => await sut.GetGeneralInfo());
        }
    }

}
